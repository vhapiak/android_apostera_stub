package com.apostera.stub.ui.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.adson.controlflow.ControlProvider;
import com.adson.controlflow.command.AnalyticsCommand;
import com.adson.controlflow.command.OpenContentCommand;
import com.adson.controlflow.structures.ContentLocation;
import com.adson.controlflow.structures.OpenContentAction;
import com.adson.controlflow.structures.OpenContentActionType;
import com.adson.controlflow.structures.analytics.AnalyticsChunkEvent;
import com.adson.controlflow.structures.analytics.AnalyticsEvent;
import com.apostera.stub.R;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    private ControlProvider controlProvider = new ControlProvider.Builder().buildAdsonStub();

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.main_fragment, container, false);

        Button openButton = result.findViewById(R.id.openApp);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenAppAction(view);
            }
        });


        Button sendButton = result.findViewById(R.id.sendAnalytics);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSendAnalyticsAction(view);
            }
        });

        return result;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Intent intent = getActivity().getIntent();

        ContentLocation action = controlProvider.handle(intent, ContentLocation.class);
        try {
            if (action == null) {
                mViewModel = new MainViewModel();
            } else {
                mViewModel = new MainViewModel(action.getContentFilePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onOpenAppAction(View view) {
        if (!mViewModel.hasPOIs()) {
            showToast();
            return;
        }
        int contentId = mViewModel.getRandomId();
        OpenContentAction action = new OpenContentAction(contentId, OpenContentActionType.TEXT);

        OpenContentCommand command = new OpenContentCommand(action);

        controlProvider.open(getActivity(), command);
    }

    private void onSendAnalyticsAction(View view) {
        if (!mViewModel.hasPOIs()) {
            showToast();
            return;
        }
        int contentId = mViewModel.getRandomId();
        AnalyticsEvent shown = new AnalyticsEvent(contentId, "shown");
        AnalyticsEvent hidden = new AnalyticsEvent(contentId, "hidden");

        AnalyticsEvent[] list = {shown, hidden};
        AnalyticsChunkEvent chunk = new AnalyticsChunkEvent(list);

        AnalyticsCommand command = new AnalyticsCommand(chunk);

        controlProvider.send(getActivity(), command);

        Toast toast = Toast.makeText(getContext(), "Data sent", Toast.LENGTH_LONG);
        toast.show();
    }

    private void showToast() {
        Toast toast = Toast.makeText(getContext(), "POI DB is empty", Toast.LENGTH_LONG);
        toast.show();
    }

}
