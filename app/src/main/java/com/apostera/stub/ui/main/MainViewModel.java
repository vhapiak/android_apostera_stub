package com.apostera.stub.ui.main;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainViewModel extends ViewModel {

    private static final String LOG_TAG = "AposteraStub";

    private Document mDocument;

    public MainViewModel() throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        mDocument = builder.newDocument();
    }

    public MainViewModel(String poiDbPath) throws IOException, SAXException, ParserConfigurationException {
        Log.d(LOG_TAG, "Path to POI db: " + poiDbPath);

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        mDocument = builder.parse(new FileInputStream(poiDbPath));

        NodeList pois = mDocument.getElementsByTagName("poi");
        Log.d(LOG_TAG, "Number of parsed POIs: " + pois.getLength());
    }

    public boolean hasPOIs() {
        return mDocument.getElementsByTagName("id").getLength() != 0;
    }

    public int getRandomId() {
        NodeList ids = mDocument.getElementsByTagName("id");
        int randomIndex = (int)(Math.random() * ids.getLength());

        String idStr = ids.item(randomIndex).getFirstChild().getNodeValue();
        int id = Integer.parseInt(idStr);
        Log.d(LOG_TAG, "Random POI id: " + id);

        return id;
    }
}
